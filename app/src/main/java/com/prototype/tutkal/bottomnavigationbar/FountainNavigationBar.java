package com.prototype.tutkal.bottomnavigationbar;

import android.animation.ValueAnimator;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.util.Log;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.AnticipateOvershootInterpolator;
import android.view.animation.BounceInterpolator;
import android.widget.RelativeLayout;

import androidx.interpolator.view.animation.FastOutLinearInInterpolator;

public class FountainNavigationBar
{
    int pos;
    int lastPosition = 0;
    private Path mPath;
    private Paint mPaint;
    /** the CURVE_CIRCLE_RADIUS represent the radius of the fab button */
    public final int CURVE_CIRCLE_RADIUS = 90;
    // the coordinates of the first curve
    public Point mFirstCurveStartPoint = new Point();
    public Point mFirstCurveEndPoint = new Point();
    public Point mFirstCurveControlPoint2 = new Point();
    public Point mFirstCurveControlPoint1 = new Point();
    //the coordinates of the second curve
    @SuppressWarnings("FieldCanBeLocal")
    public Point mSecondCurveStartPoint = new Point();
    public Point mSecondCurveEndPoint = new Point();
    public Point mSecondCurveControlPoint1 = new Point();
    public Point mSecondCurveControlPoint2 = new Point();
    public int mNavigationBarWidth;
    public int mNavigationBarHeight;
    private int barWidth,barHeight;


    private boolean init = false;
FountainNavigation fountainNavigation;

    public FountainNavigationBar(FountainNavigation fountainNavigation) {
        this.fountainNavigation = fountainNavigation;
    }

    public void Initialize(int barWidth, int barHeight) {
        this.barWidth = barWidth;
        this.barHeight = barHeight;
        mPath = new Path();
        mPaint = new Paint();
        mPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        mPaint.setColor(Color.RED);
        mPaint.setShadowLayer(30, 0, 0, Color.GRAY);
        mPaint.setAntiAlias(true);
        init = true;

    }


    public void OnDraw(Canvas canvas)
    {

        Calculate();
        RelativeLayout bar = fountainNavigation.findViewById(R.id.content);

        int deneme = bar.getHeight()-barHeight;
        if (init&canvas != null) {
            mPath.reset();
            mPath.moveTo(0, deneme);
            mPath.lineTo(mFirstCurveStartPoint.x, mFirstCurveStartPoint.y+deneme);

            mPath.cubicTo(mFirstCurveControlPoint1.x, mFirstCurveControlPoint1.y+deneme,
                    mFirstCurveControlPoint2.x, mFirstCurveControlPoint2.y+deneme,
                    mFirstCurveEndPoint.x, mFirstCurveEndPoint.y+deneme);
            mPath.cubicTo(mSecondCurveControlPoint1.x, mSecondCurveControlPoint1.y+deneme,
                    mSecondCurveControlPoint2.x, mSecondCurveControlPoint2.y+deneme,
                    mSecondCurveEndPoint.x, mSecondCurveEndPoint.y+deneme);
            mPath.lineTo(mNavigationBarWidth, deneme);
            mPath.lineTo(mNavigationBarWidth, mNavigationBarHeight+deneme);
            mPath.lineTo(0, mNavigationBarHeight+deneme);
            mPath.close();

            canvas.drawPath(mPath, mPaint);
        }
    }

    private void Calculate()
    {

        mNavigationBarWidth = barWidth;
        mNavigationBarHeight = barHeight;
        // the coordinates (x,y) of the start point before curve
        mFirstCurveStartPoint.set((pos) -(CURVE_CIRCLE_RADIUS * 2) - (CURVE_CIRCLE_RADIUS / 3), 0);
        // the coordinates (x,y) of the end point after curve
        mFirstCurveEndPoint.set(pos, CURVE_CIRCLE_RADIUS + (CURVE_CIRCLE_RADIUS / 4));
        // same thing for the second curve
        mSecondCurveStartPoint = mFirstCurveEndPoint;
        mSecondCurveEndPoint.set((pos) + (CURVE_CIRCLE_RADIUS * 2) + (CURVE_CIRCLE_RADIUS / 3), 0);
// the coordinates (x,y) of the 1st control point on a cubic curve
        mFirstCurveControlPoint1.set(mFirstCurveStartPoint.x + CURVE_CIRCLE_RADIUS + (CURVE_CIRCLE_RADIUS / 4), mFirstCurveStartPoint.y);
        // the coordinates (x,y) of the 2nd control point on a cubic curve
        mFirstCurveControlPoint2.set(mFirstCurveEndPoint.x - (CURVE_CIRCLE_RADIUS * 2) + CURVE_CIRCLE_RADIUS, mFirstCurveEndPoint.y);
        mSecondCurveControlPoint1.set(mSecondCurveStartPoint.x + (CURVE_CIRCLE_RADIUS * 2) - CURVE_CIRCLE_RADIUS, mSecondCurveStartPoint.y);
        mSecondCurveControlPoint2.set(mSecondCurveEndPoint.x - (CURVE_CIRCLE_RADIUS + (CURVE_CIRCLE_RADIUS / 4)), mSecondCurveEndPoint.y);
    }


    public void Change(final int x)
    {
        ValueAnimator animator = ValueAnimator.ofInt(lastPosition, x);
        animator.setDuration(500);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {

               pos =   (int)animation.getAnimatedValue();

                fountainNavigation.invalidate();

            }
        });
        animator.setInterpolator(new AnticipateOvershootInterpolator());
        animator.start();
        lastPosition = x;
    }

    ///endregion

}
