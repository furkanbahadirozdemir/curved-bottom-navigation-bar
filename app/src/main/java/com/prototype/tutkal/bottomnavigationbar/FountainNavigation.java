package com.prototype.tutkal.bottomnavigationbar;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.view.menu.MenuBuilder;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class FountainNavigation extends FrameLayout
{

    public ImageView[] buttons = new ImageView[5];
    public ImageView circle;
    private RelativeLayout mContentView;

    FountainNavigationMenu menu;

    FountainNavigationBar bar;
    public FountainNavigation(Context context) {
        this(context, null);
    }

    public FountainNavigation(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }


    public FountainNavigation(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        LayoutInflater.from(context).inflate(R.layout.layout, this);
        mContentView = (RelativeLayout) findViewById(R.id.content);


        Menu deneme = CreateMenu(context,attrs,defStyle);
        menu = new FountainNavigationMenu(context,deneme,this);
        bar = new FountainNavigationBar(this);

        menu.SetListener(new IFountainNavigationClick() {
            @Override
            public void OnClick(int itemPosition) {
                bar.Change(itemPosition);
            }
        });
        Initialize();








    }

    @Override
    public void addView(View child, int index, ViewGroup.LayoutParams params) {
        if(mContentView == null){
            super.addView(child, index, params);
        } else {
            //Forward these calls to the content view
            mContentView.addView(child, index, params);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        bar.OnDraw(canvas);

    }

    private Menu CreateMenu(Context context,AttributeSet attrs,int defStyle)
    {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.FountainNavigation, defStyle, 0);
        Menu menu = new MenuBuilder(context);

        new MenuInflater(context).inflate( a.getResourceId(R.styleable.FountainNavigation_menu,0), menu);
        a.recycle();
        return menu;
    }


    private void Initialize()
    {
        final LinearLayout a = FountainNavigation.this.findViewById(R.id.menu);
        a.post(new Runnable(){
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            public void run() {

                menu.Initialize(a.getWidth(),a.getHeight());
                bar.Initialize(a.getWidth(),a.getHeight());

            }
        });
    }








}
