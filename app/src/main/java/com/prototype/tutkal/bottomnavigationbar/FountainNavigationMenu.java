package com.prototype.tutkal.bottomnavigationbar;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnticipateOvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Space;

import androidx.annotation.RequiresApi;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class FountainNavigationMenu
{

    private int barWidth,barHeight;
    private float buttonStartY;
    private FountainNavigation fountainNavigation;
    private Menu menu;

    Context context;
    public ImageView[] buttons = new ImageView[6];
    public Button circle;

    int lastPosition;

    IFountainNavigationClick clickListener;

    private View selectedButton = null;


    public FountainNavigationMenu(Context context,Menu menu,FountainNavigation fountainNavigation) {
        this.menu = menu;
        this.fountainNavigation= fountainNavigation;
        this.context = context;


    }


    public void Initialize(int barWidth, int barHeight)
    {
        this.barWidth = barWidth;
        this.barHeight= barHeight;
        CreateElement();
    }

    public void SetListener(IFountainNavigationClick clickListener)
    {
        this.clickListener = clickListener;
    }

    private void CreateElement()
    {
        int screenWidth = barWidth;
        int screenHeight = barHeight;
        screenWidth -= (screenHeight/3*2)*menu.size();

        int sidePadding = screenWidth/10*2;
        int buttonPadding = (int) (screenWidth / 10 * ((float) 6 / ((float) (menu.size()-1))) );



        for (int i = 0 ; i<menu.size();i++)
        {

            if (i == 0)
                HelperCreateSpace(sidePadding);
            else if (i<=menu.size()-1)
                HelperCreateSpace(buttonPadding);

            buttons[i] =  HelperCreateButton(screenHeight/3*2,screenHeight/3*2,menu.getItem(i).getIcon());


        }
        buttons[0].post(new Runnable() {
            @Override
            public void run() {

                buttonStartY = buttons[0].getY();
                Log.d("asfasf",buttons[0].getY()+"");
                HelperCreateCircle();
            }
        });


    }



    private void ClickButton(final View v)
    {
        for (ImageView btn :buttons)
        {
            if (selectedButton != null && selectedButton != btn)
            {
                Animation(selectedButton,buttonStartY-80,buttonStartY,1,.8f);
                break;
            }
        }
        Animation(v,buttonStartY,buttonStartY-80,.8f,1);
        selectedButton = v;
        int[] location = new int[2];
        v.getLocationOnScreen(location);
        final int x = location[0];
        final int y = location[1];



        v.post(new Runnable() {
            @Override
            public void run() {
                CircleMove(((int) v.getX()+ (v.getWidth()/2))-(circle.getWidth()/2));
                circle.setY(30);

                clickListener.OnClick((int) v.getX()+ (v.getWidth()/2));
            }
        });

    }

    ///region Animation
    private void Animation(final View item, float start, float end, float startScale, final float targetScale)
    {
        Log.d("asfasf",buttonStartY+"");
        ValueAnimator animator = ValueAnimator.ofFloat(start, end);
        animator.setDuration(500);
        animator.setInterpolator(new AnticipateOvershootInterpolator());
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {

                float k  = (float)animation.getAnimatedValue();
                item.setY(k);
            }
        });
        animator.start();

        ScaleAnimation fade_in = new ScaleAnimation(startScale, targetScale, startScale, targetScale, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        fade_in.setDuration(500);
        fade_in.setFillAfter(true);
        item.startAnimation(fade_in);
    }

    private void CircleMove( int targetX)
    {
        ValueAnimator animator = ValueAnimator.ofFloat(circle.getX(), targetX);
        animator.setDuration(500);
        animator.setInterpolator(new AnticipateOvershootInterpolator());
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {

                float k  = (float)animation.getAnimatedValue();
                circle.setX(k);
            }
        });
        animator.start();
    }
    ///endregion

    ///region Helper
    private ImageView HelperCreateButton(int width, int height, Drawable icon)
    {
        final ImageView btn = new ImageView(context);
        btn.setLayoutParams(new LinearLayout.LayoutParams(125,125));
        btn.setImageDrawable(icon);
        btn.setY((barHeight-125)/2);
        btn.setScaleX(.8f);
        btn.setScaleY(.8f);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClickButton(v);
            }
        });
        LinearLayout asa = fountainNavigation.findViewById(R.id.menu);
        asa.addView(btn);


        return btn;
    }

    private void HelperCreateCircle()
    {
        Button btn = new Button(context);
        btn.setLayoutParams(new RelativeLayout.LayoutParams(200,200));
        btn.setBackground(fountainNavigation.getResources().getDrawable(R.drawable.shape_circle));

        int[] location = new int[2];
        buttons[0].getLocationOnScreen(location);
        int x = location[0];
        int y = location[1];
        btn.setY(0);
        btn.setX(0);
        btn.setZ(-100);
        btn.setTranslationZ(-100);


        fountainNavigation.addView(btn);

        circle = btn;



    }
    private void HelperCreateSpace(int width)
    {
        Space space = new Space(context);

        space.setLayoutParams(new LinearLayout.LayoutParams(width,0));
        LinearLayout asa = fountainNavigation.findViewById(R.id.menu);
        asa.addView(space);
    }
    ///endregion




}
